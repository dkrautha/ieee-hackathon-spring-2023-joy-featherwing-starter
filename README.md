# IEEE Hackathon Spring 2023 Joy FeatherWing Starter

## Board Information

A small extension board with a 2-axis joystick and five buttons on it (4 large,
1 small).

It talks to a host microcontroller over I2C, so it doesn't take up any analog or
digital pins, and there's also an interrupt pin (which we won't go into detail
about here).

You can find more cool information here: <https://www.adafruit.com/product/3632>

## Soldering The Pins

The board comes without its pins actually soldered on, so you get to do it
yourself! I had the best success by sticking the pins long side down into a
breadboard, and laying the FeatherWing on top. Just take it slow, and you'll
have the pins done in no time. For the time being, don't worry about soldering
an interrupt pin or the address change pins on the bottom, but they could be
something fun to look into in the future.

## Testing The Arduino

Before we do any wiring let's make sure your Arduino is working. I'm going to
assume you have the Arduino IDE for this process. Arduino recently released a
version 2.0 of their IDE, so I'd recommend going for that if you don't have a
1.x version already.

Link: <https://docs.arduino.cc/software/ide-v2>

Create a new sketch, and paste the following inside:

```cpp
// taken from https://docs.arduino.cc/built-in-examples/basics/Blink
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);  // turn the LED on (HIGH is the voltage level)
  delay(1000);                      // wait for a second
  digitalWrite(LED_BUILTIN, LOW);   // turn the LED off by making the voltage LOW
  delay(1000);                      // wait for a second
}
```

Plug in your Arduino board, and select board from the "Select Board" dropdown.
Finally, press the upload button (a circular button with an arrow facing to the
right), and the code should be compiled and uploaded to the Arduino.

Once the process finishes, the onboard LED should blink on and off every second.
If that happens you know your board is working!

## Installing A Library to Use the FeatherWing

Click the library icon on the left side of the screen:

![Installing a Library](./installing-a-library-img01.png)

A list will appear of all available libraries where you can search for the
necessary library, which in this case is called "Adafruit seesaw". Click the
install button and give it a minute or so to install.

![Library Search Page](./installing-a-library-img02.png)

Once its finished, it should show as installed in the left hand column.

![Installed Library](./installing-a-library-img04.png)

## Wiring Diagram

![Wiring Diagram](./joywing_wiring_bb.png)

The board you have won't look exactly identical to this, but the pin wiring is
the same. As long as you have the blue wire pin going into SCL, the yellow wire
pin going into SDA, and 3.3v and ground wired correctly you'll be fine. Feel
free to ask us for any help if you need it!

## FeatherWing Code

```cpp
#include "Adafruit_seesaw.h"

Adafruit_seesaw ss;

#define BUTTON_RIGHT 6
#define BUTTON_DOWN 7
#define BUTTON_LEFT 9
#define BUTTON_UP 10
#define BUTTON_SEL 14
uint32_t button_mask = (1 << BUTTON_RIGHT) | (1 << BUTTON_DOWN) |
                       (1 << BUTTON_LEFT) | (1 << BUTTON_UP) |
                       (1 << BUTTON_SEL);

#if defined(ESP8266)
#define IRQ_PIN 2
#elif defined(ESP32) && !defined(ARDUINO_ADAFRUIT_FEATHER_ESP32S2)
#define IRQ_PIN 14
#elif defined(ARDUINO_NRF52832_FEATHER)
#define IRQ_PIN 27
#elif defined(TEENSYDUINO)
#define IRQ_PIN 8
#elif defined(ARDUINO_ARCH_WICED)
#define IRQ_PIN PC5
#else
#define IRQ_PIN 5
#endif

void setup() {
  Serial.begin(9600);

  while (!Serial) {
    delay(10);
  }

  Serial.println("Joy FeatherWing example!");

  if (!ss.begin(0x49)) {
    Serial.println("ERROR! seesaw not found");
    while (1)
      delay(100);
  } else {
    Serial.println("seesaw started");
    Serial.print("version: ");
    Serial.println(ss.getVersion(), HEX);
  }
  ss.pinModeBulk(button_mask, INPUT_PULLUP);
  ss.setGPIOInterrupts(button_mask, 1);

  pinMode(IRQ_PIN, INPUT);
}

int last_x = 0, last_y = 0;

void loop() {
  int x = ss.analogRead(2);
  int y = ss.analogRead(3);

  if ((abs(x - last_x) > 3) || (abs(y - last_y) > 3)) {
    Serial.print(x);
    Serial.print(", ");
    Serial.println(y);
    last_x = x;
    last_y = y;
  }

  /* if(!digitalRead(IRQ_PIN)) {  // Uncomment to use IRQ */

  uint32_t buttons = ss.digitalReadBulk(button_mask);

  // Serial.println(buttons, BIN);

  if (!(buttons & (1 << BUTTON_RIGHT))) {
    Serial.println("Button A pressed");
  }
  if (!(buttons & (1 << BUTTON_DOWN))) {
    Serial.println("Button B pressed");
  }
  if (!(buttons & (1 << BUTTON_LEFT))) {
    Serial.println("Button Y pressed");
  }
  if (!(buttons & (1 << BUTTON_UP))) {
    Serial.println("Button X pressed");
  }
  if (!(buttons & (1 << BUTTON_SEL))) {
    Serial.println("Button SEL pressed");
  }
  /* } // Uncomment to use IRQ */
  delay(10);
}
```

Compile and upload, then check the serial console output in the IDE and you
should see the X and Y readings of the joystick, and if you press the buttons
that should show up as well.

If you have any questions on what is going on come find an eboard member
(especially me! (David K)) and we'll try and explain what you're looking at :).
